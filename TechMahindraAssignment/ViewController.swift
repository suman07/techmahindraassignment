//
//  ViewController.swift
//  TechMahindraAssignment
//
//  Created by Sudipta on 19/07/20.
//  Copyright © 2020 Sudipta. All rights reserved.
//

import UIKit
import SnapKit
class ViewController: UIViewController {
    
    
    var arrRowdata = [RowViewModel]()
    lazy var containerView = UIView()
    lazy var tableView = UITableView()
    var navBarHeight = 44
    var gap = CGFloat ()
    var navTitle : String? = nil
    var navigationBar = UINavigationBar()
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /// Using snapkit framework programetically creating a view and adding tableview,  Here i adding leading, trailing,top, bottom,height and width.
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.gap = 10.0
        self.view.addSubview(containerView)
        containerView.snp.makeConstraints { make in
            make.left.right.equalTo(self.view)
            make.top.equalTo(navBarHeight)
            make.top.equalTo(self.view.snp.top).offset(navBarHeight)
            make.bottom.equalTo(self.view.snp.bottom).offset(0)
        }
        containerView.addSubview(tableView)
        tableView.snp.makeConstraints { make in
            
            make.top.equalTo(gap)
            make.height.equalTo(self.view.snp.height).multipliedBy(0.89)
            make.left.equalTo(self.view.snp.left).offset(gap)
            make.right.equalTo(self.view.snp.right).offset(-gap)
            tableView.backgroundColor = UIColor.clear
            self.tableView.register(TableViewCell.self, forCellReuseIdentifier: TableViewCell.identifier)
            tableView.delegate = self
            tableView.dataSource = self
            tableView.estimatedRowHeight = 50
            tableView.allowsSelection = true
            tableView.tableFooterView = UIView()
            self.tableView.isUserInteractionEnabled = true
        }
        self.networkCall()
        
        
    }
    // MARK: NetworkCall
    /// returns: string , array of dictionary, and Error.
    func networkCall() {
        Service.shred.getdatafromServer {
            (data, Title, error) in
            if data != nil
            {
                self.arrRowdata =  data?.map({return RowViewModel(rowModeldata: $0)}) ?? []
                
                DispatchQueue.main.async {
                    //  self.navigationController?.navigationBar.sizeToFit()
                    // self.navigationController?.navigationBar.topItem?.title = Title
                    
                    self.navTitle = Title
                    self.navigationBar.topItem?.title = self.navTitle
                    
                    self.tableView.reloadData()
                }
            }
            
        }
    }
    /// Create NavigationBar and Navigation item.
    override func viewWillLayoutSubviews() {
        let width = self.view.frame.width
        navigationBar = UINavigationBar(frame: CGRect(x: 0, y: 20, width: width, height: 44))
        self.view.addSubview(navigationBar);
        let navigationItem = UINavigationItem(title: self.navTitle ?? "")
        let doneBtn = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.refresh, target: nil, action: #selector(buttonRefresh))
        navigationItem.rightBarButtonItem = doneBtn
        navigationBar.setItems([navigationItem], animated: false)
        
    }
    
    /// Call webservicce
    @objc func buttonRefresh() {
        self.networkCall()
    }
}
   // MARK: TableViewDelegate,TableViewDataSource Method
extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrRowdata.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCell.identifier, for: indexPath) as! TableViewCell
        let data = self.arrRowdata[indexPath.row]
        cell.iconImageView.loadImageUsingCache(withUrl: data.imageHref ?? "")
        cell.labelTitle.text = data.title
        cell.labelTitle.snp.updateConstraints{ (make) in
            
            make.height.equalTo(cell.labelTitle.heightForLabel(text:  data.title ?? "", font: BoldFont17, width: cell.contentView.frame.width))
            
        }
        cell.labelDbescription.text = data.description
        cell.labelDbescription.snp.updateConstraints{ (make) in
            
            make.top.equalTo(cell.labelTitle.snp.bottom).offset(1)
            make.bottom.equalTo(cell.contentView.snp.bottom)
        }
        return cell
    }
}

