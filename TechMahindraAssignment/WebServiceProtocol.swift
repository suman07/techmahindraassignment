//
//  WebServiceProtocol.swift
//  TechMahindraAssignment
//
//  Created by Sudipta on 20/07/20.
//  Copyright © 2020 Sudipta. All rights reserved.
//

import Foundation
protocol WebServiceProtocol {
        func getdatafromServer(completion: @escaping ([RowsModel]?,String?,Error?) -> Void)
}
