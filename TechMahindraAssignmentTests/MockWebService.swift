//
//  MockWebService.swift
//  TechMahindraAssignment
//
//  Created by Sudipta on 20/07/20.
//  Copyright © 2020 Sudipta. All rights reserved.
//

import Foundation
class MockWebService: WebServiceProtocol {
    var isMethodCalled: Bool = false
    var shouldReturnError: Bool = false
 
   func getdatafromServer(completion: @escaping ([RowsModel]?, String?, webserviceError?) -> Void) {
        
        isMethodCalled = true
        
        if shouldReturnError {
            completion(nil,"", webserviceError.failedRequest(description: "Rrequest was not successful"))
        }
 
    }

}
