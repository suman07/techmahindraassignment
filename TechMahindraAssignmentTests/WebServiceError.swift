//
//  WebServiceError.swift
//  TechMahindraAssignment
//
//  Created by Sudipta on 20/07/20.
//  Copyright © 2020 Sudipta. All rights reserved.
//

import Foundation
enum webserviceError: LocalizedError, Equatable {
    
    case invalidResponseModel
    case invalidRequestURLString
    case failedRequest(description: String)
    
    var errorDescription: String? {
        switch self {
        case .failedRequest(let description):
            return description
        case .invalidResponseModel, .invalidRequestURLString:
            return ""
        }
    }
    
}
